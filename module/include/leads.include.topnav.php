<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container-fluid">

            <!-- Logo container-->
            <div class="logo">
                <!-- Text Logo -->
                <!-- <a href="index.html" class="logo">
                    <span class="logo-small"><i class="mdi mdi-radar"></i></span>
                    <span class="logo-large"><i class="mdi mdi-radar"></i> Highdmin</span>
                </a> -->
                <!-- Image Logo -->
                <a href="#" class="logo">
                    <img src="../../public/images/logo_sm.png" alt="" height="26" class="logo-small">
                    <img src="../../public/images/MarcaHorizontalCores02.png" alt="" height="40" class="logo-large">
                </a>

            </div>
            <!-- End Logo container-->


            <div class="menu-extras topbar-custom">

                <ul class="list-unstyled topbar-right-menu float-right mb-0">

                    <li class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>


                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <img src="../../public/data/uploads/perfil/<?php echo $imagem?>" alt="user" class="rounded-circle"> <span class="ml-1"><?php echo $nomeLogado ?> <i class="mdi mdi-chevron-down"></i> </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h6 class="text-overflow m-0"><script language="JavaScript">
                                        var myDate = new Date(); if ( myDate.getHours() < 12 )
                                        { document.write("Bom dia!");
                                        }else  /* Hour is from noon to 5pm (actually to 5:59 pm) */
                                        if ( myDate.getHours() >= 12 && myDate.getHours() <= 17 )
                                        { document.write("Boa tarde!");
                                        } else  /* the hour is after 5pm, so it is between 6pm and midnight */
                                        if ( myDate.getHours() > 17 && myDate.getHours() <= 24 )
                                        { document.write("Boa noite!"); } else  /* the hour is not between 0 and 24, so something is wrong */
                                        { document.write("I'm not sure what time it is!"); }
                                    </script> </h6>
                            </div>


                            <!--                    <a href="#" class="dropdown-item notify-item">-->
                            <!--                        <i class="fi-head"></i> <span>Minha Conta</span>-->
                            <!--                    </a>-->

                            <!--                    <!-- item-->
                            <!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
                            <!--                        <i class="fi-cog"></i> <span>Configurações</span>-->
                            <!--                    </a>-->
                            <!---->
                            <!--                    <!-- item-->
                            <!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
                            <!--                        <i class="fi-help"></i> <span>Support</span>-->
                            <!--                    </a>-->
                            <!---->
                            <!--                    <!-- item-->
                            <!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
                            <!--                        <i class="fi-lock"></i> <span>Lock Screen</span>-->
                            <!--                    </a>-->

                            <!-- item-->
                            <a href="?sair" onclick="return confirm('Deseja realmente sair?');" class="dropdown-item notify-item">
                                <i class="fi-power"></i> <span>Logout</span>
                            </a>

                        </div>
                    </li>
                </ul>
            </div>
            <!-- end menu-extras -->

            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="../view/view.leads.php"><i class="icon-speedometer"></i>Dashboard</a>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class="icon-layers"></i>Leads</a>
                        <ul class="submenu">
                            <li><a href="../view/view.leads.php">Plano de Saúde</a></li>

                        </ul>
                    </li>




                    <li class="has-submenu">
                        <a href="#"><i class="icon-docs"></i>Landing Pages</a>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/amil" target="_blank">Amil</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/amil/amilrj.php" target="_blank">Amil RJ</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/bradesco" target="_blank">Bradesco</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/intermedica" target="_blank">Intermédica</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/intermedica/intermedicarj.php" target="_blank">Intermédica RJ</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/sulamerica" target="_blank">Sulamérica</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/sulamerica/sulamericarj.php" target="_blank">Sulamérica RJ</a></li>

                                </ul>
                            </li>
                            <li>
                                <ul>

                                    <li><a href="https://landingpages.planosdesaude360.com.br/saudetmz" target="_blank">Genérica</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/next" target="_blank">Next</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/next/nextrj.php" target="_blank">Next RJ</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/biovida" target="_blank">Biovida</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/trasmontano" target="_blank">Trasmontano</a></li>
                                    <li><a href="https://landingpages.planosdesaude360.com.br/medsenior" target="_blank">MedSênior</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>


                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->
