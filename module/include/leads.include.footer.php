<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                2018 © TMZ - agenciatresmeiazero.com.br
            </div>
        </div>
    </div>
</footer>


<!-- jQuery  -->
<script src="../../public/js/jquery.min.js"></script>
<script src="../../public/js/popper.min.js"></script>
<script src="../../public/js/bootstrap.min.js"></script>
<script src="../../public/js/waves.js"></script>
<script src="../../public/js/jquery.slimscroll.js"></script>
<script src="../../public/js/sweetalert2.all.js"></script>

<!-- Flot chart -->
<script src="../../public/plugins/flot-chart/jquery.flot.min.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.time.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.resize.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.pie.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.crosshair.js"></script>
<script src="../../public/plugins/flot-chart/curvedLines.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.axislabels.js"></script>
<!-- Required datatable js -->
<script src="../../public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../public/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="../../public/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="../../public/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="../../public/plugins/datatables/jszip.min.js"></script>
<script src="../../public/plugins/datatables/pdfmake.min.js"></script>
<script src="../../public/plugins/datatables/vfs_fonts.js"></script>
<script src="../../public/plugins/datatables/buttons.html5.min.js"></script>
<script src="../../public/plugins/datatables/buttons.print.min.js"></script>

<!-- Key Tables -->
<script src="../../public/plugins/datatables/dataTables.keyTable.min.js"></script>

<!-- Responsive examples -->
<script src="../../public/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="../../public/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!-- Selection table -->
<script src="../../public/plugins/datatables/dataTables.select.min.js"></script>
<!-- KNOB JS -->
<!--[if IE]>
<script type="text/javascript" src="../../public/plugins/jquery-knob/excanvas.js"></script>
<![endif]-->
<script src="../../public/plugins/jquery-knob/jquery.knob.js"></script>

<!-- Dashboard Init -->
<script src="../../public/pages/jquery.dashboard.init.js"></script>
<!--FooTable-->
<script src="../../public/plugins/footable/js/footable.all.min.js"></script>
<!--FooTable Example-->
<script src="../../public/pages/jquery.footable.js"></script>
<!-- App js -->
<script src="../../public/js/jquery.core.js"></script>
<script src="../../public/js/jquery.app.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        // Default Datatable
        $('#datatable').DataTable({

        });

        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: true,
            paging:         false,
            searching: true,
            buttons: ['copy','excel', 'pdf'],
            info: true
        });

        // Key Tables

        $('#key-table').DataTable({
            keys: true
        });

        // Responsive Datatable
        $('#responsive-datatable').DataTable();

        // Multi Selection Datatable
        $('#selection-datatable').DataTable({
            select: {
                style: 'multi'
            }
        });

        table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    } );

</script>
