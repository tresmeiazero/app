<?php
require_once("../../config/dbconnect.php");
include_once("../../module/controllers/verifica.usuario.logado.php");

?>
<!DOCTYPE html>
<html>

<?php include_once("../../module/include/leads.include.header.php"); ?>

    <body>

    <?php include_once("../../module/include/leads.include.topnav.php"); ?>


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">TMZ</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">

                    <div class="col-md-12 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Bradesco<img class="pull-right" src="../../public/images/logo-bradesco.png" width="30"></h4>
                            <?php include_once ("../../module/include/leads.saude.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.bradesco.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>
                        </div>
                    </div>




                </div>



            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

    <?php include_once("../../module/include/leads.include.footer.php"); ?>


    </body>
</html>