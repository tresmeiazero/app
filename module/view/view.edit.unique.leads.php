<?php
// load up your config file
require_once("../../config/dbconnect.php");
include_once("../../module/controllers/verifica.usuario.logado.php");

// RECUPERA DADOS
if(!isset($_GET['id'])){
    header("Location: view.leads.php"); exit;
}
$id = $_GET['id'];
$select = "SELECT * from tmzleadsgeral WHERE strId=:id";
$contagem =1;
try{
    $result = $conexao->prepare($select);
    $result->bindParam(':id', $id, PDO::PARAM_INT);
    $result->execute();
    $contar = $result->rowCount();
    if($contar>0){
        while($show = $result->FETCH(PDO::FETCH_OBJ)){

            $nome            = $show->nome;
            $email  	     = $show->email;
            $telefone         = $show->telefone;
            $telefoneAlternativo  = $show->telefoneAlternativo;
            $possuicnpj      = $show->possuicnpj;
            $cnpj            = $show->cnpj;
            $estado          = $show->estado;
            $cidade          = $show->cidade;
            $quantidadefamiliar      = $show->quantidadefamiliar;
            $quantidadepme     = $show->quantidadepme ;
            $operadora       = $show->operadora;
            $operadoraAmil       = $show->operadoraAmil;
            $operadoraBradesco       = $show->operadoraBradesco;
            $operadoraIntermedica       = $show->operadoraIntermedica;
            $mensagem        = $show->mensagem;
            $tipodeplano        = $show->tipodeplano;
            $tipopessoa        = $show->tipopessoa;
            $modalidade        = $show->modalidade;
        }
    }else{
        echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                         <strong>Aviso</strong> Não existem dados cadastrados com o id informado.
                                        </div>';exit;
    }
}catch(PDOException $e){
    $msgErroID = $e;
}
// ATUALIZA
if(isset($_POST['atualizar'])){

  $nome            = trim(strip_tags($_POST['nome']));
  $email           = trim(strip_tags($_POST['email']));
  $telefone         = trim(strip_tags($_POST['telefone']));
  $telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
  $possuicnpj      = trim(strip_tags($_POST['possuicnpj']));
  $cnpj            = trim(strip_tags($_POST['cnpj']));
  $estado          = trim(strip_tags($_POST['estado']));
  $cidade          = trim(strip_tags($_POST['cidade']));
  $quantidadepme      = trim(strip_tags($_POST['quantidadepme']));
  $quantidadefamiliar     = trim(strip_tags($_POST['quantidadefamiliar']));
  $operadora       = trim(strip_tags($_POST['operadora']));
  $operadoraAmil      = trim(strip_tags($_POST['operadoraAmil']));
  $operadoraBradesco     = trim(strip_tags($_POST['operadoraBradesco']));
  $operadoraIntermedica       = trim(strip_tags($_POST['operadoraIntermedica']));
  $operadoraSamed       = trim(strip_tags($_POST['operadoraSamed']));
  $operadoraBiovida       = trim(strip_tags($_POST['operadoraBiovida']));
  $operadoraTrasmontano       = trim(strip_tags($_POST['operadoraTrasmontano']));
  $operadoraSulamerica       = trim(strip_tags($_POST['operadoraSulamerica']));
  $operadoraNext       = trim(strip_tags($_POST['operadoraNext']));
  $mensagem        = trim(strip_tags($_POST['mensagem']));
  $tipodeplano       = trim(strip_tags($_POST['tipodeplano']));
  $tipopessoa      = trim(strip_tags($_POST['tipopessoa']));
  $modalidade     = trim(strip_tags($_POST['modalidade']));

    $update = "UPDATE tmzleadsgeral SET  nome=:nome, email=:email, telefone=:telefone, telefoneAlternativo=:telefoneAlternativo, possuicnpj=:possuicnpj, cnpj=:cnpj, estado=:estado, cidade=:cidade, quantidadepme=:quantidadepme, quantidadefamiliar=:quantidadefamiliar, operadora=:operadora, operadoraAmil=:operadoraAmil, operadoraBradesco=:operadoraBradesco, operadoraIntermedica=:operadoraIntermedica, operadoraSamed=:operadoraSamed, operadoraBiovida=:operadoraBiovida, operadoraTrasmontano=:operadoraTrasmontano, operadoraSulamerica=:operadoraSulamerica, operadoraNext=:operadoraNext, mensagem=:mensagem, tipodeplano=:tipodeplano, tipopessoa=:tipopessoa, modalidade=:modalidade WHERE strId=:id";

    try{
        $result = $conexao->prepare($update);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':nome', $nome, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
        $result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
        $result->bindParam(':possuicnpj', $possuicnpj, PDO::PARAM_STR);
        $result->bindParam(':cnpj', $cnpj, PDO::PARAM_STR);
        $result->bindParam(':estado', $estado, PDO::PARAM_STR);
        $result->bindParam(':cidade', $cidade, PDO::PARAM_STR);
        $result->bindParam(':quantidadepme', $quantidadepme, PDO::PARAM_STR);
        $result->bindParam(':quantidadefamiliar', $quantidadefamiliar, PDO::PARAM_STR);
        $result->bindParam(':operadora', $operadora, PDO::PARAM_STR);
        $result->bindParam(':operadoraAmil', $operadoraAmil, PDO::PARAM_STR);
        $result->bindParam(':operadoraBradesco', $operadoraBradesco, PDO::PARAM_STR);
        $result->bindParam(':operadoraIntermedica', $operadoraIntermedica, PDO::PARAM_STR);
        $result->bindParam(':operadoraSamed', $operadoraSamed, PDO::PARAM_STR);
        $result->bindParam(':operadoraBiovida', $operadoraBiovida, PDO::PARAM_STR);
        $result->bindParam(':operadoraTrasmontano', $operadoraTrasmontano, PDO::PARAM_STR);
        $result->bindParam(':operadoraSulamerica', $operadoraSulamerica, PDO::PARAM_STR);
        $result->bindParam(':operadoraNext', $operadoraNext, PDO::PARAM_STR);
        $result->bindParam(':mensagem', $mensagem, PDO::PARAM_STR);
        $result->bindParam(':tipodeplano', $tipodeplano, PDO::PARAM_STR);
        $result->bindParam(':tipopessoa', $tipopessoa, PDO::PARAM_STR);
        $result->bindParam(':modalidade', $modalidade, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
            $msgAtualizaClientesSucesso =  '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                 <strong>Sucesso.</strong> Os dados do usuário foram atualizados
                                           </div>'; echo "<script type='text/javascript'>
                setTimeout(function () {
                window.location.href = \"view.leads.php\";
            }, 2000);  </script>";
        }else{
            $msgAtualizaClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               <strong>Erro!</strong> Não foi possível atualizar os dados deste usuário, tente novamente.
                                         </div>';
        }
    }catch(PDOException $e){
        $error = $e;
    }

}

?>
<!DOCTYPE html>
<html>

<?php include_once("../../module/include/leads.include.header.php"); ?>

<body>

<?php include_once("../../module/include/leads.include.topnav.php"); ?>



<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">TMZ</a></li>
                            <li class="breadcrumb-item"><a href="#">Leads</a></li>
                            <li class="breadcrumb-item"><a href="#">Visão Geral</a></li>
                            <li class="breadcrumb-item active"><?php echo $nome;?></li>
                        </ol>
                    </div>
                    <h4 class="page-title">Visão Geral</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title">Lead id #<?php echo $id;?></h4>
                    <br>
                    <?php echo $error; ?>
                    <?php echo $msgErroID; ?>
                    <?php echo $msgAtualizaClientesSucesso; ?>
                    <?php echo $msgAtualizaClientesErro; ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="p-20">
                                <form  role="form" class="form-signin" action="#" method="post">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Nome</label>
                                        <div class="col-10">
                                            <input class="form-control" name="nome" id="nome"  value="<?php echo $nome;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label" for="example-email">E-mail</label>
                                        <div class="col-10">
                                            <input  class="form-control" name="email" id="email"  value="<?php echo $email;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Telefone</label>
                                        <div class="col-10">
                                            <input class="form-control" name="telefone" id="telefone"  value="<?php echo $telefone;?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Telefone Alternativo</label>
                                        <div class="col-10">
                                            <input class="form-control" name="telefoneAlternativo" id="telefoneAlternativo"  value="<?php echo $telefoneAlternativo;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Possui CNPJ ?</label>
                                        <div class="col-10">
                                            <input  class="form-control" name="possuicnpj" id="possuicnpj"  value="<?php echo $possuicnpj;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">CNPJ</label>
                                        <div class="col-10">
                                            <input class="form-control" name="cnpj" id="cnpj"  value="<?php echo $cnpj;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Modalidade</label>
                                        <div class="col-10">
                                            <input  class="form-control" name="modalidade" id="modalidade"  value="<?php echo $modalidade;?>">
                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Estado</label>
                                        <div class="col-10">
                                            <input class="form-control" name="estado" id="estado" value="<?php echo $estado;?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Cidade</label>
                                        <div class="col-10">
                                            <input class="form-control" name="cidade" id="cidade" value="<?php echo $cidade;?>">
                                        </div>
                                    </div>





                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Observações</label>
                                        <div class="col-10">
                                            <textarea class="form-control" id="mensagem" name="mensagem"  rows="5">

                                              <?php echo $quantidadefamiliar;?> <?php echo $quantidadepme;?>  <?php echo $modalidade;?> | <?php echo $possuicnpj;?> | <?php echo $tipopessoa;?> <?php echo $cnpj;?>  | <?php echo $mensagem;?> </textarea>
                                        </div>
                                    </div>
                                    <button type="submit" name="atualizar" class="btn btn-primary">Atualizar</button>
                                    <a href="view.unique.leads.php" class="btn btn-success waves-light waves-effect">Voltar</a>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->

                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>


    </div> <!-- end container -->
</div>



<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                2018 © TMZ - agenciatresmeiazero.com.br
            </div>
        </div>
    </div>
</footer>


<!-- jQuery  -->
<script src="../../public/js/jquery.min.js"></script>
<script src="../../public/js/popper.min.js"></script>
<script src="../../public/js/bootstrap.min.js"></script>
<script src="../../public/js/waves.js"></script>
<script src="../../public/js/jquery.slimscroll.js"></script>

<!-- Flot chart -->
<script src="../../public/plugins/flot-chart/jquery.flot.min.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.time.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.resize.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.pie.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.crosshair.js"></script>
<script src="../../public/plugins/flot-chart/curvedLines.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.axislabels.js"></script>
<!-- Required datatable js -->
<script src="../../public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../public/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="../../public/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="../../public/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="../../public/plugins/datatables/jszip.min.js"></script>
<script src="../../public/plugins/datatables/pdfmake.min.js"></script>
<script src="../../public/plugins/datatables/vfs_fonts.js"></script>
<script src="../../public/plugins/datatables/buttons.html5.min.js"></script>
<script src="../../public/plugins/datatables/buttons.print.min.js"></script>

<!-- Key Tables -->
<script src="../../public/plugins/datatables/dataTables.keyTable.min.js"></script>

<!-- Responsive examples -->
<script src="../../public/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="../../public/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!-- Selection table -->
<script src="../../public/plugins/datatables/dataTables.select.min.js"></script>
<!-- KNOB JS -->
<!--[if IE]>
<script type="text/javascript" src="../../public/plugins/jquery-knob/excanvas.js"></script>
<![endif]-->
<script src="../../public/plugins/jquery-knob/jquery.knob.js"></script>

<!-- Dashboard Init -->
<script src="../../public/pages/jquery.dashboard.init.js"></script>
<!--FooTable-->
<script src="../../public/plugins/footable/js/footable.all.min.js"></script>
<!--FooTable Example-->
<script src="../../public/pages/jquery.footable.js"></script>
<!-- App js -->
<script src="../../public/js/jquery.core.js"></script>
<script src="../../public/js/jquery.app.js"></script>

</body>
</html>
