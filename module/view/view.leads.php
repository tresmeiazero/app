<?php
    require_once("../../config/dbconnect.php");
    include_once("../../module/controllers/verifica.usuario.logado.php");

    ?>
<!DOCTYPE html>
<html>
    <?php include_once("../../module/include/leads.include.header.php"); ?>
    <body>
        <?php include_once("../../module/include/leads.include.topnav.php"); ?>
        <?php
            // excluir post
            if(isset($_GET['delete'])){
                $id_delete = $_GET['delete'];

                // exclui o registro

                $seleciona = "DELETE from tmzleadsgeral WHERE strId=:id_delete";
                try{
                    $result = $conexao->prepare($seleciona);
                    $result->bindParam('id_delete',$id_delete, PDO::PARAM_STR);
                    $result->execute();
                    $contar = $result->rowCount();
                    if($contar>0){
                        $usuarioDeletadoSucesso = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                           Usuário deletado com <strong>Sucesso! </strong>
                                                    </div>'; echo "<script type='text/javascript'>
                            setTimeout(function () {
                            window.location.href = \"view.leads.php\";
                        }, 2000);  </script>";
                    }else{
                        $usuarioDeletadoErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Erro!</strong> Não foi possível excluir o usuario.
                                                  </div>';
                    }
                }catch (PDOWException $erro){ echo $erro;}


            }

            ?>
        <div class="wrapper">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item ">TMZ</li>
                                    <li class="breadcrumb-item ">Leads</li>
                                    <li class="breadcrumb-item active">Visão Geral</li>
                                </ol>
                            </div>
                            <h6>Visão Geral > <span style="color: #000;"> Leads </span></h6>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->



                <div class="row">
                          <div class="col-md-12 col-xs-12">
                        <div class="card-box table-responsive">
                            <?php echo $usuarioDeletadoSucesso; ?>
                            <?php echo $usuarioDeletadoErro; ?>
                            <table id="datatable-buttons" class="table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Observação</th>
                                        <th>Modalidade</th>
                                        <th>Nome</th>
                                        <th>E-mail</th>
                                        <th>FonePrincipal</th>
                                        <th>FoneCelular</th>
                                        <th>Cidade</th>
                                        <th>IdFonte</th>
                                        <th>Operadora</th>
                                        <th>Data</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if(empty($_GET['pg'])){}
                                        else{ $pg = $_GET['pg'];
                                            if(!is_numeric($pg)){
                                                echo '<script language="JavaScript">
                                                           location.href=" ViewClientesVisualizar.php"; </script>';
                                            }

                                        }
                                        if(isset($pg)){ $pg = $_GET['pg'];}else{ $pg = 1;}

                                        $quantidade = 1000;
                                        $inicio = ($pg*$quantidade) - $quantidade;
                                        $select = "SELECT * from tmzleadsgeral ORDER BY strId DESC LIMIT $inicio, $quantidade";
                                        try {
                                            $result = $conexao->prepare($select);
                                            $result->execute();
                                            $contar = $result->rowCount();
                                            if($contar>0){
                                                while($show = $result->FETCH(PDO::FETCH_OBJ)){
                                                    date_default_timezone_set('America/Sao_Paulo');
                                                    $date = date_create($show->strData);
                                                    $date = date_format($date, 'd-m-Y H:i');

                                                    ?>
                                    <tr>
                                        <td><?php echo $show->quantidadefamiliar;?>  <?php echo $show->quantidadepme;?>  <?php echo $show->modalidade;?> ; <?php echo $show->possuicnpj;?> ; <?php echo $show->tipopessoa;?> <?php echo $show->cnpj;?>  ;  <?php echo $show->mensagem;?></td>
                                        <td><?php echo $show->modalidade;?></td>
                                        <td><?php echo $show->nome;?></td>
                                        <td><?php echo $show->email;?></td>
                                        <td><?php echo $show->telefone;?></td>
                                        <td><?php echo $show->telefoneAlternativo;?></td>
                                        <td><?php echo $show->cidade;?></td>
                                        <td></td>
                                        <td>
                                          <span class="badge label-table badge-purple"><?php echo $show->operadora;?></span>
                                          <span class="badge label-table badge-danger"><?php echo $show->operadoraBradesco;?></span>
                                          <span class="badge label-table badge-info"><?php echo $show->operadoraAmil;?></span>
                                          <span class="badge label-table badge-warning"><?php echo $show->operadoraIntermedica;?></span>
                                          <span class="badge label-table badge-success"><?php echo $show->operadoraSamed;?></span>
                                          <span class="badge label-table badge-info"><?php echo $show->operadoraBiovida;?></span>
                                          <span class="badge label-table badge-secondary"><?php echo $show->operadoraTrasmontano;?></span>
                                          <span class="badge label-table badge-warning"><?php echo $show->operadoraSulamerica;?></span>
                                          <span class="badge label-table badge-pink"><?php echo $show->operadoraNext;?></span>
                                          <span class="badge label-table badge-warning"><?php echo $show->operadoraGoldencross;?></span>
                                          <span class="badge label-table badge-success"><?php echo $show->operadoraMedSenior;?></span>
                                        </td>
                                        <td><?php echo date('d/m/Y H:i', strtotime($date . ' - 3 hour '));?></td>
                                        <td><a href="view.leads.php?pg=<?php echo $pg;?>&delete=<?php echo $show->strId;?>" onClick="return confirm('Deseja realmente excluir este usuário ?')" id="confirmaExclusao"  class="icon-user-unfollow btn btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deletar Usuário"> </a>
                                            <a href="view.unique.leads.php?id=<?php echo $show->strId;?>" class="icon-eye btn btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar Usuário"> </a>
                                            <a href="view.edit.unique.leads.php?id=<?php echo $show->strId;?>" class="icon-pencil btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar Usuário">
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                        }else{
                                        echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                        Desculpe, não existem dados cadastrados no momento !
                                                </div>';
                                        }
                                        }catch(PDOException $e){
                                        echo $e;
                                        }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("../../module/include/leads.include.footer.php"); ?>

    </body>
</html>
