<?php
// load up your config file
require_once("../../config/dbconnect.php");
include_once("../../module/controllers/verifica.usuario.logado.php");


if(!isset($_GET['id'])){
    header("Location: view.leads.php"); exit;
    exit; }
$id = $_GET['id'];
$select = "SELECT * FROM tmzleadsgeral WHERE strId=:id";
$contagem =1;
try {
$result = $conexao->prepare($select);
$result->bindParam(':id',$id, PDO::PARAM_INT);
$result->execute();
$contar = $result->rowCount();
if($contar>0){
while($show = $result->FETCH(PDO::FETCH_OBJ)){
$id = $show->id;
$date = date_create($show->strData);
$date = date_format($date, 'd-m-Y');
$dateToday = date('d-m-Y', strtotime("1 days"));

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Sistema TMZ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="../../public/images/favicon.ico">

    <!-- App css -->
    <link href="../../public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../public/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../../public/css/style.css" rel="stylesheet" type="text/css" />

    <script src="../../public/js/modernizr.min.js"></script>

</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container-fluid">

            <!-- Logo container-->
            <div class="logo">
                <!-- Text Logo -->
                <!-- <a href="index.html" class="logo">
                    <span class="logo-small"><i class="mdi mdi-radar"></i></span>
                    <span class="logo-large"><i class="mdi mdi-radar"></i> Highdmin</span>
                </a> -->
                <!-- Image Logo -->
                <a href="#" class="logo">
                    <img src="../../public/images/logo_sm.png" alt="" height="26" class="logo-small">
                    <img src="../../public/images/MarcaHorizontalCores02.png" alt="" height="40" class="logo-large">
                </a>

            </div>
            <!-- End Logo container-->


            <div class="menu-extras topbar-custom">

                <ul class="list-unstyled topbar-right-menu float-right mb-0">

                    <li class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>


                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <img src="../../public/data/uploads/perfil/<?php echo $imagem?>" alt="user" class="rounded-circle"> <span class="ml-1"><?php echo $nomeLogado ?> <i class="mdi mdi-chevron-down"></i> </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h6 class="text-overflow m-0"><script language="JavaScript">
                                        var myDate = new Date(); if ( myDate.getHours() < 12 )
                                        { document.write("Bom dia!");
                                        }else  /* Hour is from noon to 5pm (actually to 5:59 pm) */
                                        if ( myDate.getHours() >= 12 && myDate.getHours() <= 17 )
                                        { document.write("Boa tarde!");
                                        } else  /* the hour is after 5pm, so it is between 6pm and midnight */
                                        if ( myDate.getHours() > 17 && myDate.getHours() <= 24 )
                                        { document.write("Boa noite!"); } else  /* the hour is not between 0 and 24, so something is wrong */
                                        { document.write("I'm not sure what time it is!"); }
                                    </script> </h6>
                            </div>


                            <!--                    <a href="#" class="dropdown-item notify-item">-->
                            <!--                        <i class="fi-head"></i> <span>Minha Conta</span>-->
                            <!--                    </a>-->

                            <!--                    <!-- item-->
                            <!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
                            <!--                        <i class="fi-cog"></i> <span>Configurações</span>-->
                            <!--                    </a>-->
                            <!---->
                            <!--                    <!-- item-->
                            <!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
                            <!--                        <i class="fi-help"></i> <span>Support</span>-->
                            <!--                    </a>-->
                            <!---->
                            <!--                    <!-- item-->
                            <!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
                            <!--                        <i class="fi-lock"></i> <span>Lock Screen</span>-->
                            <!--                    </a>-->

                            <!-- item-->
                            <a href="?sair" onclick="return confirm('Deseja realmente sair?');" class="dropdown-item notify-item">
                                <i class="fi-power"></i> <span>Logout</span>
                            </a>

                        </div>
                    </li>
                </ul>
            </div>
            <!-- end menu-extras -->

            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="../view/home.php"><i class="icon-speedometer"></i>Dashboard</a>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class="icon-layers"></i>Leads</a>
                        <ul class="submenu">
                            <li><a href="../view/view.leads.php">Plano de Saúde</a></li>
                            <li><a href="#">Biovida</a></li>
                        </ul>
                    </li>



                    <li class="has-submenu">
                        <a href="#"><i class="icon-docs"></i>Landing Pages</a>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li><a href="http://landingpages.planosdesaude360.com.br/LandingPageAmil.php" target="_blank">Amil</a></li>
                                    <li><a href="http://landingpages.planosdesaude360.com.br/bradesco" target="_blank">Bradesco</a></li>
                                    <li><a href="http://landingpages.planosdesaude360.com.br/LandingPageIntermedica.php" target="_blank">Intermédica</a></li>
                                    <li><a href="http://landingpages.planosdesaude360.com.br/LandingPageSulamerica.php">Sulamérica</a></li>

                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li><a href="http://landingpages.planosdesaude360.com.br/LandingPageGenerica.php" target="_blank">Genérica</a></li>
                                    <li><a href="http://landingpages.planosdesaude360.com.br/LandingPageNext.php" target="_blank">Next</a></li>
                                    <li><a href="http://landingpages.planosdesaude360.com.br/biovida" target="_blank">Biovida</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>


                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">TMZ</a></li>
                            <li class="breadcrumb-item"><a href="#">Leads</a></li>
                            <li class="breadcrumb-item"><a href="#">Visão Geral</a></li>
                            <li class="breadcrumb-item active"><?php echo $show->nome;?></li>
                        </ol>
                    </div>
                    <h4 class="page-title">Visão Geral</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title">Lead ID #<?php echo $show->strId;?></h4>
<br>

                    <div class="row">
                        <div class="col-12">
                            <div class="p-20">
                              <form  role="form" class="form-signin" action="#" method="post">
                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Nome</label>
                                      <div class="col-10">
                                          <input class="form-control" name="nome" id="nome"  value="<?php echo $show->nome;?>">
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-2 col-form-label" for="example-email">E-mail</label>
                                      <div class="col-10">
                                          <input  class="form-control" name="email" id="email"  value="<?php echo $show->email;?>">
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Telefone</label>
                                      <div class="col-10">
                                          <input class="form-control" name="telefone" id="telefone"  value="<?php echo $show->telefone;?>">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Telefone Alternativo</label>
                                      <div class="col-10">
                                          <input class="form-control" name="telefoneAlternativo" id="telefoneAlternativo"  value="<?php echo $show->telefoneAlternativo;?>">
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Possui CNPJ ?</label>
                                      <div class="col-10">
                                          <input  class="form-control" name="possuicnpj" id="possuicnpj"  value="<?php echo $show->possuicnpj;?>">
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">CNPJ</label>
                                      <div class="col-10">
                                          <input class="form-control" name="cnpj" id="cnpj"  value="<?php echo $show->cnpj;?>">
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Modalidade</label>
                                      <div class="col-10">
                                          <input  class="form-control" name="modalidade" id="modalidade"  value="<?php echo $show->modalidade;?>">
                                      </div>
                                  </div>



                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Estado</label>
                                      <div class="col-10">
                                          <input class="form-control" name="estado" id="estado" value="<?php echo $show->estado;?>">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Cidade</label>
                                      <div class="col-10">
                                          <input class="form-control" name="cidade" id="cidade" value="<?php echo $show->cidade;?>">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Vidas PME</label>
                                      <div class="col-10">
                                          <input type="text" class="form-control" id="modalidade" name="modalidade"  value="<?php echo $show->modalidade;?>">
                                      </div>
                                  </div>


                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Operadora</label>
                                      <div class="col-10">
                                          <input class="form-control" name="operadora" id="operadora"  value="<?php echo $show->operadora;?><?php echo $show->operadoraAmil;?>">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="col-2 col-form-label">Observações</label>
                                      <div class="col-10">
                                          <textarea class="form-control" id="mensagem" name="mensagem"  rows="5"><?php echo $show->quantidadefamiliar;?> <?php echo $show->quantidadepme;?>  <?php echo $show->modalidade;?> | <?php echo $show->possuicnpj;?> | <?php echo $show->tipopessoa;?> <?php echo $show->cnpj;?>  | <?php echo $show->mensagem;?> </textarea>
                                      </div>
                                  </div>
                                  <a href="view.leads.php" class="btn btn-success waves-light waves-effect">Voltar</a>
                              </form>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->

                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>


    </div> <!-- end container -->
</div>

<?php
}
}else{
    echo '<div class="alert media fade in alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                          <strong>Aviso!</strong> Ainda não existem posts cadastrados. </div>';
}
}catch(PDOException $e){
    echo $e;
}
?>


<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                2018 © TMZ - agenciatresmeiazero.com.br
            </div>
        </div>
    </div>
</footer>


<!-- jQuery  -->
<script src="../../public/js/jquery.min.js"></script>
<script src="../../public/js/popper.min.js"></script>
<script src="../../public/js/bootstrap.min.js"></script>
<script src="../../public/js/waves.js"></script>
<script src="../../public/js/jquery.slimscroll.js"></script>
<script src="../../public/js/sweetalert2.all.js"></script>

<!-- Flot chart -->
<script src="../../public/plugins/flot-chart/jquery.flot.min.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.time.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.resize.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.pie.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.crosshair.js"></script>
<script src="../../public/plugins/flot-chart/curvedLines.js"></script>
<script src="../../public/plugins/flot-chart/jquery.flot.axislabels.js"></script>
<!-- Required datatable js -->
<script src="../../public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../public/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="../../public/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="../../public/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="../../public/plugins/datatables/jszip.min.js"></script>
<script src="../../public/plugins/datatables/pdfmake.min.js"></script>
<script src="../../public/plugins/datatables/vfs_fonts.js"></script>
<script src="../../public/plugins/datatables/buttons.html5.min.js"></script>
<script src="../../public/plugins/datatables/buttons.print.min.js"></script>

<!-- Key Tables -->
<script src="../../public/plugins/datatables/dataTables.keyTable.min.js"></script>

<!-- Responsive examples -->
<script src="../../public/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="../../public/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!-- Selection table -->
<script src="../../public/plugins/datatables/dataTables.select.min.js"></script>
<!-- KNOB JS -->
<!--[if IE]>
<script type="text/javascript" src="../../public/plugins/jquery-knob/excanvas.js"></script>
<![endif]-->
<script src="../../public/plugins/jquery-knob/jquery.knob.js"></script>

<!-- Dashboard Init -->
<script src="../../public/pages/jquery.dashboard.init.js"></script>
<!--FooTable-->
<script src="../../public/plugins/footable/js/footable.all.min.js"></script>
<!--FooTable Example-->
<script src="../../public/pages/jquery.footable.js"></script>
<!-- App js -->
<script src="../../public/js/jquery.core.js"></script>
<script src="../../public/js/jquery.app.js"></script>

</body>
</html>
