<?php
ob_start();
session_start();
if(isset($_SESSION['usuariosistema']) && isset($_SESSION['senhasistema'])){
    header("Location: module/view/view.leads.php");exit;
}
require_once("config/dbconnect.php");

if(isset($_GET['acao'])){

    if(!isset($_POST['logar'])){
        $acao = $_GET['acao'];
        if($acao=='negado'){
            $erroLogar = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Erro ao acessar!</strong> <br>Você precisa estar logado p/ acessar o sistema.
                                    </div>';
        }
    }
}
if(isset($_POST['logar'])){

    //Recuperar Dados do Form
    $usuario = trim(strip_tags($_POST['usuario']));
    $senha = trim(strip_tags($_POST['senha']));

    //Selecionar Banco de Dados
    $select = "SELECT * from tmzleadsusers WHERE BINARY usuario=:usuario AND BINARY senha=:senha";

    try{
        $result = $conexao->prepare($select);
        $result->bindParam(':usuario', $usuario, PDO::PARAM_STR);
        $result->bindParam(':senha', $senha, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
            $usuario = $_POST['usuario'];
            $senha = $_POST['senha'];
            $_SESSION['usuariosistema'] = $usuario;
            $_SESSION['senhasistema'] = $senha;
            $loginsucesso = '<div class="alert alert-success alert-dismissable alert-style-1">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<i class="zmdi zmdi-check"></i>Yay! Logado com sucesso!
										</div>';

            echo "<script type='text/javascript'>
                setTimeout(function () {
                window.location.href = \"module/view/view.leads.php?acao=welcome\";
            }, 2000);  </script>";

        }else{
            $loginerro = '<div class="alert alert-danger alert-dismissable alert-style-1">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<i class="zmdi zmdi-block"></i>Opps! Dados incorretos
										</div>';
        }
    }catch(PDOException $e){
        echo $e;
    }
}//Se Clicar no Botão Entra no Sistema
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>Sistema de Gestão - Agência 360</title>
		<meta name="author" content="hencework"/>

		<!-- Favicon -->
		<link rel="shortcut icon" href="public/login/favicon.ico">
		<link rel="icon" href="public/login/favicon.ico" type="image/x-icon">

		<!-- vector map CSS -->
		<link href="public/login/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>

		<!-- Custom CSS -->
		<link href="public/login/dist/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->

		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="#">
						<img class="brand-img mr-10" src="public/login/dist/img/logo.png" alt="brand"/>
						<span class="brand-text">TresMeiaZero</span>
					</a>
				</div>
							<div class="clearfix"></div>
			</header>

			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sistema TMZ</h3>
											<h6 class="text-center nonecase-font txt-grey">Preencha seus dados para entrar</h6>
										</div>
                                        <?php echo $loginsucesso; ?>
                                        <?php echo $loginerro; ?>
                                        <?php echo $erroLogar; ?>
										<div class="form-wrap">
                                            <form class="form-signin" action="#" method="post" enctype="multipart/form-data">
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputEmail_2">Usuário</label>
													<input type="text" class="form-control" required="" id="usuario" name="usuario" placeholder="Insira seu usuário">
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_2">Senha</label>
													<div class="clearfix"></div>
													<input type="password" class="form-control" required="" id="senha" name="senha" placeholder="Insira sua senha">
												</div>


												<div class="form-group text-center">
													<button type="submit" class="btn btn-info  btn-rounded" name="logar">Entrar</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

		<script src="public/login/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="public/login/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="public/login/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
        <script src="public/login/dist/js/jquery.slimscroll.js"></script>
		<script src="public/login/dist/js/init.js"></script>
	</body>
</html>
